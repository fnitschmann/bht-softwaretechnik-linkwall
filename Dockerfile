FROM golang:1.18.0-alpine3.15 as base

RUN apk add git g++ gcc make bash bash-completion openssh-client

RUN adduser -D -g "default application user" appuser

RUN wget -O- -nv https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.45.2
WORKDIR /src
COPY . .

FROM base AS builder

RUN CGO_ENABLED=0 GOOS=linux go build -a -o app

FROM scratch

USER appuser

COPY --from=builder /src/app /app
COPY --from=builder /src/database/migrations /database/migrations/
COPY --from=builder /etc/ssl/certs /etc/ssl/certs

EXPOSE 8080

CMD ["/app"]
