package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	// set up signal handling
	_, cancel := context.WithCancel(context.Background())

	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM)
	go func() {
		s := <-signalCh
		fmt.Printf("received %s - shutting down", s)
		cancel()
	}()

	fmt.Println("Starting BHT Link Collector Application. Soon more to come.")
}
